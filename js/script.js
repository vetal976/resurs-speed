window.onload = function () {
	//document.getElementsByTagName("button")[0].addEventListener("click", makeRequest, false); 
	document.getElementsByTagName("button")[0].onclick = makeRequest;
}

function makeRequest(event){	
	var form = document.getElementsByTagName("form")[0];

	if(1){
		/*1-й способ ($_POST)*/
		console.log('1 способ');				
		request ='PS=' + form.PS.value + '&Rmax=' + form.Rmax.value + '&Vpr=' + form.Vpr.value + '&Fp=' + form.Fp.value;	
		sendRequest(request);
		//	sendRequest(new FormData(form));			
	} else {
		/*2-й способ (php://input)*/
		console.log('2 способ');
		var request = {
			PS: form.PS.value,
			Rmax: form.Rmax.value,
			Vpr: form.Vpr.value,
			Fp: form.Fp.value
		};
		request = JSON.stringify(request);
		sendRequest(request, 'application/json');		
	}
}

function sendRequest (request, contentType) {
	var contentType = contentType || 'application/x-www-form-urlencoded';
	
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	    var xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
			document.getElementById("result").innerHTML = xmlhttp.responseText;
		}
	}
	
	xmlhttp.open("POST","server/index.php", true);
	xmlhttp.setRequestHeader('Content-Type', contentType);
	xmlhttp.send(request);
}