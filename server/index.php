<?php
require("lib.php");
//print_r($_POST); exit;

if (count($_POST) != 0){
    /*1-й способ*/
    $Rmax = $_POST['Rmax'];//задане максимальне навантаження
    $Vpr = $_POST['Vpr'];//швидкість приземлення
    $Fp = $_POST['Fp'];//площа купола
    $PS = $_POST['PS'];    
} else {
    /*2-й способ*/
    $requestBody = file_get_contents('php://input');
    $req = json_decode($requestBody);
    $Rmax = $req->Rmax;
    $Vpr = $req->Vpr;
    $Fp = $req->Fp;
    $PS = $req->PS;
}

//значення коефіцієнтів Сп і К
$Cp_arr = array(0.8, 0.9, 1);	
$K_arr = array(0.005, 0.0056, 0.006);	

//відносна щільність повітря для різних висот
$delta_arr = array('100'=>0.99043, '1000'=>0.90743, '4000'=>0.66857, '8000'=>0.42855, '12000'=>0.25357);

//розрахунок максимальної початкової швидкості для заданого максимального навантаження
$data = countV0($Rmax, $Vpr, $Fp, $Cp_arr, $K_arr, $delta_arr);

//вивід результатів 
header("Content-type: text/plain");
echo '<div style = "text-align: center;">'."Парашутна система ".$PS.'</div>'; 
printResult2($data);