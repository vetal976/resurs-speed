<?php
/* -------------- библиотека функций ----------------------- */ 
function countV0($Rmax, $Vpr, $Fp, $Cp_arr, $K_arr, $delta_arr)
{
	$data = array();
	foreach($delta_arr as $key=>$delta)
	{			
		foreach($K_arr as $K)
		{
			foreach($Cp_arr as $Cp)
			{
				$V0 = sqrt(8*$Rmax*($K*pow($Vpr, 2)/$delta+2*sqrt($Fp))/($K*pow($Vpr, 2)*$Cp*$Fp)-sqrt($Fp)/$K);
				$data["$key"]["$K"]["$Cp"] = $V0;				
			}			
		}		
	}
	return $data;
}

function printResult($data)
{
	foreach($data as $H=>$K_arr)
	{	
		echo '<div style = "clear: both; padding-top: 20px;">';
		echo '<div>'."Значення максимальної початкової швидкості системи для висоти Н = ".$H." м".'</div>'; echo '<br>';
		foreach($K_arr as $K=>$Cp_arr)
		{
			
			echo '<div style = "border: 1px dotted grey; float: left; margin: 4px 5px; padding: 8px;">';
			echo "При К = ".$K; echo '<br>';
			echo '<table rules ="cols">'.'<tr>'.'<td>Cп</td>';
			foreach($Cp_arr as $Cp=>$V)
			{
				echo '<td>'.$Cp.'</td>';			
			}
			echo '</tr>'.'<tr>'.'<td>Vo, м/с</td>';
			foreach($Cp_arr as $Cp=>$V)
			{
				echo '<td>';
				printf("%.2f", $V); 
				echo '</td>';
			}
			echo '</tr>';
			echo '</table>';
			echo '</div>';
		}
		echo '</div>';
	}
}

function printResult2($data)
{
	global $Rmax;
	$tableNumber = 1;
	echo '<div style = "text-align: center;">'."Значення максимальної швидкості системи в момент початку наповнення при заданому максимальному навантаженні Rmax = ".$Rmax." кгс".'</div>'; 
	foreach($data as $H=>$K_arr)
	{	
		echo '<div style = "clear: both; padding-top: 20px;">';
		echo '<div>'."Таблиця {$tableNumber} - Значення швидкості для висоти Н = ".$H." м".'</div>'; //echo '<br>';
		echo '<table rules ="cols">';
			echo '<tr>';
				echo '<td></td>';
				foreach($K_arr as $K=>$Cp_arr)
				{	
							
					echo '<td colspan = 3>'."При К = ".$K.'</td>'; 				
				}
			echo '</tr>';	
			
			echo '<tr>';
				echo '<td>Cп</td>';
				foreach($K_arr as $K=>$Cp_arr)
				{	
					foreach($Cp_arr as $Cp=>$V)
					{
						echo '<td>'.$Cp.'</td>';			
					}					
				}
			echo '</tr>';		
			
			echo '<tr>';
				echo '<td>Vo, м/с</td>';	
				foreach($K_arr as $K=>$Cp_arr)
				{			
					foreach($Cp_arr as $Cp=>$V)
					{
						echo '<td>';
						printf("%.2f", $V); 
						echo '</td>';
					}						
				}
			echo '</tr>';
		echo '</table>';
		echo '</div>';
		$tableNumber++;
	}
}	